class CreateSearches < ActiveRecord::Migration[5.1]
  def change
    create_table :searches do |t|
      t.string :keyword, default: ''
      t.string :search_type, default: ''
      t.string :email, default: ''
      t.text :results, default: ''

      t.timestamps
    end
  end
end
