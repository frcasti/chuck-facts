class SearchesController < ApplicationController
	def index
		@categories = Chuck::Api.get_request 'categories'
		@categories << 'random'
		@search = Search.new
	end

	def create
		@search = Search.new search_params

		if !params[:search][:email].blank?
			set_session params[:search][:email]
		end

		@search.email = session[:email] unless session[:email].nil?

		if !params[:search][:category].blank?
			@results = Chuck::Api.get_request "random?category=#{params[:search][:category]}"
			@search[:results] = @results
			@search[:search_type] = params[:search][:category] == 'random' ? 'random' : 'category'
			@search[:keyword] = params[:search][:category]
		elsif !params[:search][:keyword].blank?
			puts Chuck::Api.get_request "search?query=#{params[:search][:keyword]}"
			@results = Chuck::Api.get_request "search?query=#{params[:search][:keyword]}"
			@search[:results] = @results
			@search[:search_type] = 'keyword'
			@search[:keyword] = params[:search][:keyword]
		end

    if @search.save

      ChuckMailer.send_search(session[:email], @results).deliver_now unless session[:email].nil?
      render json: { results: @results }
    end
	end

	private
		def search_params
      params.require(:search).permit(:keyword, :search_type, :email)
    end

    def set_session email
    	session[:email] = email
    end

    def remove_session
    	session[:email] = nil
    end
end
