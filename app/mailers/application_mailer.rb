class ApplicationMailer < ActionMailer::Base
  default from: 'castillo.gomez@gmail.com'
  layout 'mailer'
end
