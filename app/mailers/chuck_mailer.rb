class ChuckMailer < ApplicationMailer
	def send_search email, results
		@results = results
		mail(to: email, subject: t('.search_results'))
	end
end
