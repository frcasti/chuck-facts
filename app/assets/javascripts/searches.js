document.addEventListener("turbolinks:load", function() {
	$('.search-chuck').on('ajax:success', function(e, data, status, xhr) {
		$('.search-chuck').trigger('reset');
		console.log(data);
		if (typeof data.results !== 'undefined') {
			if (typeof data.results.total !== 'undefined') {
				$.each(data.results.result, function(index, val) {
					$('.results').append('<div class="result"><img src="'+val.icon_url+'"><span class="fact">'+val.value+'</span></div>');
				});
			}
			else {
				$('.results').append('<div class="result"><img src="'+data.results.icon_url+'"><span class="fact">'+data.results.value+'</span></div>');
			}
		}
	});
});