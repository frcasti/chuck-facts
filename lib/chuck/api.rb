require 'faraday'
require 'json'

module Chuck
  class Api
		URL = 'https://api.chucknorris.io/jokes/'

		def self.get_request term = 'random'
			response = Faraday.get(URL+term)

			JSON.parse response.body
		end

		def self.post_request terms = {}
			response = Faraday.post URL, terms

			JSON.parse response.body
		end
	end
end