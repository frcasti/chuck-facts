# Chuck Search

No me ha dado tiempo ha realizar varias cosas importantes del proyecto:

* Paginación

* Maquetación

* Pequeños ajustes del buscador

Las traducciones están configuradas aunque en inglés no he llegado a introducir muchas.

## Y estas son algunas capturas

![picture](screenshots/cap_1.png)
![picture](screenshots/cap_2.png)
![picture](screenshots/cap_3.png)
